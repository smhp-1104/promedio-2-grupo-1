﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveView : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveModel moveModel = GetComponent<MoveModel>();
        moveModel.speed = 8;
    }
}
